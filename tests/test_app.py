import json

# / → Get call which returns inmemory store
def test_view_inmemorystore(app, client):
    res = client.get('/')
    assert res.status_code == 200
    expected = {}
    assert expected == json.loads(res.get_data(as_text=True))

# /set → Post call which sets the key/value pair
def test_register_key(app, client):   
    res = client.post('/set', data = {"key" : "abc-1", "value" : "10"})
    assert res.status_code == 200
    expected = {"abc-1":"10"}
    assert expected == res.json

# /get/<key> → Return value of the key
def test_get_registered_key(app, client):   
    res = client.post('/set', data = {"key" : "abc-1", "value" : "10"})
    assert res.status_code == 200
    expected = {"abc-1":"10"}
    assert expected == res.json

    res = client.get('/get/abc-1')
    assert res.status_code == 200
    expected = "10"
    assert expected == res.get_data(as_text=True)

    res = client.get('/get/abc-10')
    assert res.status_code == 200
    expected = "Key not found"
    assert expected == res.get_data(as_text=True)

#/search → Search for keys using the following filters
#Assume you have keys: abc-1, abc-2, xyz-1, xyz-2
def test_find_keys(app, client):   
    res = client.post('/set', data = {"key" : "abc-1", "value" : "10"})
    res = client.post('/set', data = {"key" : "abc-2", "value" : "20"})
    res = client.post('/set', data = {"key" : "xyz-1", "value" : "15"})
    res = client.post('/set', data = {"key" : "xyz-2", "value" : "25"})

    #/search?prefix=abc would return abc-1 and abc-2
    res = client.get('/search?prefix=abc')
    assert res.status_code == 200
    expected = {"abc-1":"10","abc-2":"20"}
    assert expected == res.json

    #/search?suffix=-1 would return abc-1 and xyz-1
    res = client.get('/search?suffix=1')
    assert res.status_code == 200
    expected = {"abc-1":"10","xyz-1":"15"}
    assert expected == res.json

# /healthz → health-check endpoint 
def test_healthz(app, client):
    res = client.get('/healthz')
    assert res.status_code == 200
    expected = "healthy"
    assert expected == res.get_data(as_text=True)
