# key-value-store

Flask api-server for key, value store(in memory)


# How to run tests?

```
python -m pytest
```

# How to run the service?

on linux hosts:
```
$ virtualenv api-server
$ source api-server/bin/activate
$ pip install -r requirements.txt
$ python3 -m flask run
```