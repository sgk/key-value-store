from flask import Flask, request
from prometheus_flask_exporter import PrometheusMetrics

app = Flask(__name__)
inmemorystore = {}
metrics = PrometheusMetrics(app)

@app.route('/', methods=['GET'])
def view_inmemorystore():
    return inmemorystore

@app.route('/set', methods=['POST'])
def register_key():
    key = request.form.get('key')
    value = request.form.get('value')
    inmemorystore[key] = value
    return {key: value}

@app.route('/get/<key>', methods=['GET'])
def get_registered_key(key):
    if key in inmemorystore:
        return inmemorystore[key]
    return "Key not found"

@app.route('/search', methods=['GET'])
def find_keys():
    args = request.args
    if 'prefix' in args:
        keyword = args.get('prefix')
        result = {k:v for k, v in inmemorystore.items() if k.startswith(keyword)}
        return result
    if 'suffix' in args:
        keyword = args.get('suffix')
        result = {k:v for k, v in inmemorystore.items() if k.endswith(keyword)}
        return result
    return "incorrect request arg"

@app.route('/healthz', methods=['GET'])
def healthz():
    return "healthy"

if __name__ == '__main__':
    app.run()
